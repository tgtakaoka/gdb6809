/* Target-dependent code for Motorola 6809.

   Copyright (C) 1999-2013 Free Software Foundation, Inc.

   68HC11 code contributed by Stephane Carrez, stcarrez@nerim.fr
   Adapted to 6809 by Ciaran Anscomb <xroar@6809.org.uk>

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include "defs.h"
#include "frame.h"
#include "frame-unwind.h"
#include "frame-base.h"
#include "dwarf2-frame.h"
#include "trad-frame.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "gdbcmd.h"
#include "gdbcore.h"
#include "gdb_string.h"
#include "value.h"
#include "inferior.h"
#include "dis-asm.h"
#include "symfile.h"
#include "objfiles.h"
#include "arch-utils.h"
#include "regcache.h"
#include "reggroups.h"

#include "target.h"
#include "opcode/m6809.h"
/* #include "elf/m6809.h" */
/* #include "elf-bfd.h" */

/* Register numbers of various important registers.  */

#define HARD_CC_REGNUM  0
#define HARD_A_REGNUM   1
#define HARD_B_REGNUM   2
#define HARD_DP_REGNUM  3

#define HARD_X_REGNUM   4
#define HARD_Y_REGNUM   5
#define HARD_U_REGNUM   6
#define HARD_S_REGNUM   7

#define HARD_PC_REGNUM  8

#define HARD_MD_REGNUM  9
#define HARD_E_REGNUM   10
#define HARD_F_REGNUM   11
#define HARD_V_REGNUM   12

#define M6809_LAST_HARD_REG (12)

#define M6809_NUM_REGS        (13)
#define M6809_NUM_PSEUDO_REGS (0)
#define M6809_ALL_REGS        (M6809_NUM_REGS+M6809_NUM_PSEUDO_REGS)

#define M6809_REG_SIZE    (2)

struct gdbarch_tdep
{
  int dunno;
};

/*
 * Table of registers for 6809.  Also includes the additional registers in the
 * HD6309.
 */

static const char *const m6809_register_names[] = {
  "cc", "a", "b", "dp", "x", "y", "u", "s",
  "pc", "md", "e", "f", "v"
};

static const char *
m6809_register_name (struct gdbarch *gdbarch, int reg_nr)
{
  if (reg_nr < 0)
    return NULL;
  if (reg_nr >= M6809_ALL_REGS)
    return NULL;

  return m6809_register_names[reg_nr];
}

static const unsigned char *
m6809_breakpoint_from_pc (struct gdbarch *gdbarch, CORE_ADDR *pcptr,
			  int *lenptr)
{
  static unsigned char breakpoint[] = { 0x0 };

  *lenptr = sizeof (breakpoint);
  return breakpoint;
}

/* 6809 prologue analysis (none).  */

static CORE_ADDR
m6809_skip_prologue (struct gdbarch *gdbarch, CORE_ADDR pc)
{
  return pc;
}

static CORE_ADDR
m6809_unwind_pc (struct gdbarch *gdbarch, struct frame_info *next_frame)
{
  ULONGEST pc;

  pc = frame_unwind_register_unsigned (next_frame,
				       gdbarch_pc_regnum (gdbarch));
  return pc;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static void
m6809_frame_this_id (struct frame_info *this_frame,
		     void **this_prologue_cache, struct frame_id *this_id)
{
  *this_id = outer_frame_id;
}

static struct value *
m6809_frame_prev_register (struct frame_info *this_frame,
			   void **this_prologue_cache, int regnum)
{
  return frame_unwind_got_optimized (this_frame, regnum);
}

static const struct frame_unwind m6809_frame_unwind = {
  NORMAL_FRAME,
  default_frame_unwind_stop_reason,
  m6809_frame_this_id,
  m6809_frame_prev_register,
  NULL,
  default_frame_sniffer
};

static CORE_ADDR
m6809_frame_base_address (struct frame_info *this_frame, void **this_cache)
{
  return 0;
}

static CORE_ADDR
m6809_frame_args_address (struct frame_info *this_frame, void **this_cache)
{
  return 0;
}

static const struct frame_base m6809_frame_base = {
  &m6809_frame_unwind,
  m6809_frame_base_address,
  m6809_frame_base_address,
  m6809_frame_args_address
};

static CORE_ADDR
m6809_unwind_sp (struct gdbarch *gdbarch, struct frame_info *next_frame)
{
  ULONGEST sp;
  sp = frame_unwind_register_unsigned (next_frame, HARD_S_REGNUM);
  return sp;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

/* Get and print the register from the given frame.  */

static void
m6809_print_register (struct gdbarch *gdbarch, struct ui_file *file,
		      struct frame_info *frame, int regno)
{
  LONGEST rval;

  switch (regno)
    {
    case HARD_CC_REGNUM:
    case HARD_DP_REGNUM:
    case HARD_PC_REGNUM:
    case HARD_MD_REGNUM:
      rval = get_frame_register_unsigned (frame, regno);
      break;
    default:
      rval = get_frame_register_signed (frame, regno);
      break;
    }


  switch (regno)
    {
    case HARD_CC_REGNUM:
    case HARD_A_REGNUM:
    case HARD_B_REGNUM:
    case HARD_DP_REGNUM:
    case HARD_MD_REGNUM:
    case HARD_E_REGNUM:
    case HARD_F_REGNUM:
      fprintf_filtered (file, "0x%02x   ", (unsigned) rval & 0xff);
      break;
    default:
      fprintf_filtered (file, "0x%04x ", (unsigned) rval & 0xffff);
      break;
    }

  if (regno != HARD_CC_REGNUM && regno != HARD_MD_REGNUM)
    {
      print_longest (file, 'd', 1, rval);
    }

  if (regno == HARD_CC_REGNUM)
    {
      /* CC register */
      int C, Z, N, V;

      fprintf_filtered (file, "%c%c%c%c%c%c%c%c   ",
			(rval & M6809_CC_E) ? 'E' : '-',
			(rval & M6809_CC_F) ? 'F' : '-',
			(rval & M6809_CC_H) ? 'H' : '-',
			(rval & M6809_CC_I) ? 'I' : '-',
			(rval & M6809_CC_N) ? 'N' : '-',
			(rval & M6809_CC_Z) ? 'Z' : '-',
			(rval & M6809_CC_V) ? 'V' : '-',
			(rval & M6809_CC_C) ? 'C' : '-');
      N = (rval & M6809_CC_N) != 0;
      Z = (rval & M6809_CC_Z) != 0;
      V = (rval & M6809_CC_V) != 0;
      C = (rval & M6809_CC_C) != 0;

      /* Print flags.  */
      if ((C | Z) == 0)
	fprintf_filtered (file, "u> ");
      else if ((C | Z) == 1)
	fprintf_filtered (file, "u<= ");
      else if (C == 0)
	fprintf_filtered (file, "u< ");

      if (Z == 0)
	fprintf_filtered (file, "!= ");
      else
	fprintf_filtered (file, "== ");

      if ((N ^ V) == 0)
	fprintf_filtered (file, ">= ");
      else
	fprintf_filtered (file, "< ");

      if ((Z | (N ^ V)) == 0)
	fprintf_filtered (file, "> ");
      else
	fprintf_filtered (file, "<= ");
    }

  if (regno == HARD_MD_REGNUM)
    {
      fprintf_filtered (file, "%s %s %s %s ",
			(rval & H6309_MD_D0) ? "D0" : "--",
			(rval & H6309_MD_IL) ? "IL" : "--",
			(rval & H6309_MD_FM) ? "FM" : "--",
			(rval & H6309_MD_NM) ? "NM" : "--");
    }
}

/* Same as 'info reg' but prints the registers in a different way.  */
static void
m6809_print_registers_info (struct gdbarch *gdbarch, struct ui_file *file,
			    struct frame_info *frame, int regno, int cpregs)
{
  if (regno >= 0)
    {
      const char *name = gdbarch_register_name (gdbarch, regno);

      if (!name || !*name)
	return;

      fprintf_filtered (file, "%-10s ", name);
      m6809_print_register (gdbarch, file, frame, regno);
      fprintf_filtered (file, "\n");
    }
  else
    {

      fprintf_filtered (file, "PC=");
      m6809_print_register (gdbarch, file, frame, HARD_PC_REGNUM);

      fprintf_filtered (file, "\n S=");
      m6809_print_register (gdbarch, file, frame, HARD_S_REGNUM);

      fprintf_filtered (file, "\n U=");
      m6809_print_register (gdbarch, file, frame, HARD_U_REGNUM);

      fprintf_filtered (file, "\n Y=");
      m6809_print_register (gdbarch, file, frame, HARD_Y_REGNUM);

      fprintf_filtered (file, "\n X=");
      m6809_print_register (gdbarch, file, frame, HARD_X_REGNUM);

      fprintf_filtered (file, "\nDP=");
      m6809_print_register (gdbarch, file, frame, HARD_DP_REGNUM);

      fprintf_filtered (file, "\n B=");
      m6809_print_register (gdbarch, file, frame, HARD_B_REGNUM);

      fprintf_filtered (file, "\n A=");
      m6809_print_register (gdbarch, file, frame, HARD_A_REGNUM);

      fprintf_filtered (file, "\nCC=");
      m6809_print_register (gdbarch, file, frame, HARD_CC_REGNUM);

      fprintf_filtered (file, "\n V=");
      m6809_print_register (gdbarch, file, frame, HARD_V_REGNUM);

      fprintf_filtered (file, "\n F=");
      m6809_print_register (gdbarch, file, frame, HARD_F_REGNUM);

      fprintf_filtered (file, "\n E=");
      m6809_print_register (gdbarch, file, frame, HARD_E_REGNUM);

      fprintf_filtered (file, "\nMD=");
      m6809_print_register (gdbarch, file, frame, HARD_MD_REGNUM);

      fprintf_filtered (file, "\n");

    }
}

/* Return the GDB type object for the "standard" data type
   of data in register N.  */

static struct type *
m6809_register_type (struct gdbarch *gdbarch, int reg_nr)
{
  switch (reg_nr)
    {
    case HARD_CC_REGNUM:
    case HARD_A_REGNUM:
    case HARD_B_REGNUM:
    case HARD_DP_REGNUM:
    case HARD_MD_REGNUM:
    case HARD_E_REGNUM:
    case HARD_F_REGNUM:
      return builtin_type (gdbarch)->builtin_uint8;

    default:
      return builtin_type (gdbarch)->builtin_uint16;
    }
}

static int
gdb_print_insn_m6809 (bfd_vma memaddr, disassemble_info * info)
{
  //  if (info->arch == bfd_arch_m6809)
  return print_insn_m6809 (memaddr, info);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

/* 6809 register groups.
   Identify real hard registers and (not) soft registers used by gcc.  */

static struct reggroup *m6809_hard_reggroup;

static void
m6809_init_reggroups (void)
{
  m6809_hard_reggroup = reggroup_new ("hard", USER_REGGROUP);
}

static void
m6809_add_reggroups (struct gdbarch *gdbarch)
{
  reggroup_add (gdbarch, m6809_hard_reggroup);
  reggroup_add (gdbarch, general_reggroup);
  reggroup_add (gdbarch, float_reggroup);
  reggroup_add (gdbarch, all_reggroup);
  reggroup_add (gdbarch, save_reggroup);
  reggroup_add (gdbarch, restore_reggroup);
  reggroup_add (gdbarch, vector_reggroup);
  reggroup_add (gdbarch, system_reggroup);
}

static int
m6809_register_reggroup_p (struct gdbarch *gdbarch, int regnum,
			   struct reggroup *group)
{
  /* We must save the real hard register as well as gcc
     soft registers including the frame pointer.  */
  if (group == save_reggroup || group == restore_reggroup)
    {
      return (regnum <= gdbarch_num_regs (gdbarch)
	      || m6809_register_name (gdbarch, regnum));
    }

  if (group == m6809_hard_reggroup)
    {
      return regnum == HARD_CC_REGNUM || regnum == HARD_A_REGNUM
	|| regnum == HARD_B_REGNUM || regnum == HARD_DP_REGNUM
	|| regnum == HARD_X_REGNUM || regnum == HARD_Y_REGNUM
	|| regnum == HARD_U_REGNUM || regnum == HARD_S_REGNUM
	|| regnum == HARD_PC_REGNUM
	|| regnum == HARD_MD_REGNUM || regnum == HARD_E_REGNUM
	|| regnum == HARD_F_REGNUM || regnum == HARD_V_REGNUM;
    }
  return default_register_reggroup_p (gdbarch, regnum, group);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static struct gdbarch *
m6809_gdbarch_init (struct gdbarch_info info, struct gdbarch_list *arches)
{
  struct gdbarch *gdbarch;
  struct gdbarch_tdep *tdep;

  /* Need a new architecture.  Fill in a target specific vector.  */
  tdep = (struct gdbarch_tdep *) xmalloc (sizeof (struct gdbarch_tdep));
  gdbarch = gdbarch_alloc (&info, tdep);

  switch (info.bfd_arch_info->arch)
    {
    case bfd_arch_m6809:
      set_gdbarch_addr_bit (gdbarch, 16);
      set_gdbarch_num_pseudo_regs (gdbarch, M6809_NUM_PSEUDO_REGS);
      set_gdbarch_pc_regnum (gdbarch, HARD_PC_REGNUM);
      set_gdbarch_num_regs (gdbarch, M6809_NUM_REGS);
      break;

    default:
      break;
    }

  /* Initially set everything according to the ABI.
     Use 16-bit integers since it will be the case for most
     programs.  The size of these types should normally be set
     according to the dwarf2 debug information.  */
  set_gdbarch_short_bit (gdbarch, 16);
  set_gdbarch_int_bit (gdbarch, 16);
  set_gdbarch_float_bit (gdbarch, 32);
  set_gdbarch_double_bit (gdbarch, 32);
  set_gdbarch_double_format (gdbarch, floatformats_ieee_single);
  set_gdbarch_long_double_bit (gdbarch, 64);
  set_gdbarch_long_bit (gdbarch, 32);
  set_gdbarch_ptr_bit (gdbarch, 16);
  set_gdbarch_long_long_bit (gdbarch, 64);

  /* Characters are unsigned.  */
  set_gdbarch_char_signed (gdbarch, 0);

  set_gdbarch_unwind_pc (gdbarch, m6809_unwind_pc);
  set_gdbarch_unwind_sp (gdbarch, m6809_unwind_sp);

  /* Set register info.  */
  set_gdbarch_fp0_regnum (gdbarch, -1);

  set_gdbarch_sp_regnum (gdbarch, HARD_S_REGNUM);
  set_gdbarch_register_name (gdbarch, m6809_register_name);
  set_gdbarch_register_type (gdbarch, m6809_register_type);
  //set_gdbarch_pseudo_register_read(gdbarch, m6809_pseudo_register_read);
  //set_gdbarch_pseudo_register_write(gdbarch, m6809_pseudo_register_write);

  //set_gdbarch_push_dummy_call(gdbarch, m6809_push_dummy_call);

  //set_gdbarch_return_value(gdbarch, m6809_return_value);
  set_gdbarch_skip_prologue (gdbarch, m6809_skip_prologue);
  set_gdbarch_inner_than (gdbarch, core_addr_lessthan);
  set_gdbarch_breakpoint_from_pc (gdbarch, m6809_breakpoint_from_pc);
  set_gdbarch_print_insn (gdbarch, gdb_print_insn_m6809);

  m6809_add_reggroups (gdbarch);
  set_gdbarch_register_reggroup_p (gdbarch, m6809_register_reggroup_p);
  set_gdbarch_print_registers_info (gdbarch, m6809_print_registers_info);

  /* Hook in the DWARF CFI frame unwinder.  */
  dwarf2_append_unwinders (gdbarch);

  frame_unwind_append_unwinder (gdbarch, &m6809_frame_unwind);
  frame_base_set_default (gdbarch, &m6809_frame_base);

  /* Methods for saving / extracting a dummy frame's ID.  The ID's
     stack address must match the SP value returned by
     PUSH_DUMMY_CALL, and saved by generic_save_dummy_frame_tos.  */
  //set_gdbarch_dummy_id(gdbarch, m6809_dummy_id);

  /* Return the unwound PC value.  */
  set_gdbarch_unwind_pc (gdbarch, m6809_unwind_pc);

  /* Minsymbol frobbing.  */
  //  set_gdbarch_elf_make_msymbol_special(gdbarch,
  //                                       m6809_elf_make_msymbol_special);

  set_gdbarch_believe_pcc_promotion (gdbarch, 1);

  return gdbarch;
}

/* -Wmissing-prototypes */
extern initialize_file_ftype _initialize_m6809_tdep;

void
_initialize_m6809_tdep (void)
{
  register_gdbarch_init (bfd_arch_m6809, m6809_gdbarch_init);
  m6809_init_reggroups ();
}
