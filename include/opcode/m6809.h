/* m6809.h -- Header file for Motorola 6809 opcode table
   Copyright 2013
   Free Software Foundation, Inc.
   Written by Ciaran Anscomb <xroar@6809.org.uk>

   This file is part of GDB, GAS, and the GNU binutils.

   GDB, GAS, and the GNU binutils are free software; you can redistribute
   them and/or modify them under the terms of the GNU General Public
   License as published by the Free Software Foundation; either version 3,
   or (at your option) any later version.

   GDB, GAS, and the GNU binutils are distributed in the hope that they
   will be useful, but WITHOUT ANY WARRANTY; without even the implied
   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
   the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this file; see the file COPYING3.  If not, write to the Free
   Software Foundation, 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#ifndef _OPCODE_M6809_H
#define _OPCODE_M6809_H

/* Condition Code register flags.  */

#define M6809_CC_E      (0x80)	/* Entire flag (IRQ stacking) */
#define M6809_CC_F      (0x40)	/* FIRQ mask */
#define M6809_CC_H      (0x20)	/* Half carry flag */
#define M6809_CC_I      (0x10)	/* IRQ mask */
#define M6809_CC_N      (0x08)	/* Negative */
#define M6809_CC_Z      (0x04)	/* Zero */
#define M6809_CC_V      (0x02)	/* Overflow */
#define M6809_CC_C      (0x01)	/* Carry */

/* Hitachi HD6309 MD register flags. */

#define H6309_MD_D0     (0x80)	/* Divide by 0 flag */
#define H6309_MD_IL     (0x40)	/* Illegal instruction flag */
#define H6309_MD_FM     (0x02)	/* FIRQ stacking mode */
#define H6309_MD_NM     (0x01)	/* Native mode */

/*
 * 6809 operand formats as stored in the m6809_opcode table.  These flags do
 * not correspond to anything in the 6809.
 */

/* Bits 0-3: addressing mode.  */

#define M6809_ADDRMODE_MASK   (15 << 0)

#define M6809_FMT_INHERENT      (0)	/* No operand */
#define M6809_FMT_DIRECT        (1 << 0)	/* Direct page addressing */
#define M6809_FMT_IMMEDIATE8    (2 << 0)	/* 8-bit immediate value */
#define M6809_FMT_IMMEDIATE16   (3 << 0)	/* 16-bit immediate value */
#define M6809_FMT_INDEXED       (4 << 0)	/* Indexed addressing */
#define M6809_FMT_EXTENDED      (5 << 0)	/* Extended addressing */
#define M6809_FMT_RELATIVE8     (6 << 0)	/* 8-bit relative address */
#define M6809_FMT_RELATIVE16    (7 << 0)	/* 16-bit relative address */
#define M6809_FMT_REG_PAIR      (8 << 0)	/* Register pair (EXG & TFR) */
#define M6809_FMT_STACK_S       (9 << 0)	/* Push/pull register bitfield (S stack) */
#define M6809_FMT_STACK_U       (10 << 0)	/* Push/pull register bitfield (U stack) */

/* Bits 4-5: instruction page.  */

#define M6809_PAGE_MASK         (3 << 4)

#define M6809_FMT_PAGE1         (0)	/* Page 1 instruction */
#define M6809_FMT_PAGE2         (1 << 4)	/* Page 2 instruction */
#define M6809_FMT_PAGE3         (2 << 4)	/* Page 3 instruction */

/* CPU identification.  */

#define CPU6809 0x01
#define CPU6309 0x02

/* The opcode table.  */

struct m6809_opcode
{
  const char *name;		/* Op-code name */
  long format;			/* Bitfields comprised of above macros */
  unsigned int opcode;
  unsigned char arch;		/* cpu6x09 macros ORed together */
};

extern const struct m6809_opcode m6809_opcodes[];
extern const int m6809_num_opcodes;

#endif /* _OPCODE_M6809_H */
