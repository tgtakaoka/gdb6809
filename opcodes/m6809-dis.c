/* m6809-dis.c -- Motorola 6809 disassembly
   Copyright 2013
   Free Software Foundation, Inc.
   Written by Ciaran Anscomb <xroar@6809.org.uk>

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#include "sysdep.h"
#include <stdio.h>

#include "opcode/m6809.h"
#include "dis-asm.h"

#define FMT_PAGE2       M6809_FMT_PAGE2
#define FMT_PAGE3       M6809_FMT_PAGE3

#define FMT_INHERENT    M6809_FMT_INHERENT
#define FMT_DIRECT      M6809_FMT_DIRECT
#define FMT_IMM8        M6809_FMT_IMMEDIATE8
#define FMT_IMM16       M6809_FMT_IMMEDIATE16
#define FMT_IND         M6809_FMT_INDEXED
#define FMT_EXT         M6809_FMT_EXTENDED
#define FMT_REL8        M6809_FMT_RELATIVE8
#define FMT_REL16       M6809_FMT_RELATIVE16
#define FMT_REG_PAIR    M6809_FMT_REG_PAIR
#define FMT_STACK_S     M6809_FMT_STACK_S
#define FMT_STACK_U     M6809_FMT_STACK_U

static const char *const reg_indexed[] = {
  "x", "y", "u", "s"
};

static const char *const reg_pair_6809[] = {
  "d", "x", "y", "u", "s", "pc", "6", "7",
  "a", "b", "cc", "dp", "12", "13", "14", "15"
};

static const char *const reg_pair_6309[] = {
  "d", "x", "y", "u", "s", "pc", "w", "v",
  "a", "b", "cc", "dp", "0", "0", "e", "f"
};

static const char *const reg_stack[] = {
  "cc", "a", "b", "dp", "x", "y", NULL, "pc"
};

/* Prototypes for local functions.  */
static int read_memory (bfd_vma, bfd_byte *, int, struct disassemble_info *);
static int print_insn (bfd_vma, struct disassemble_info *, int);

static int
read_memory (bfd_vma memaddr, bfd_byte * buffer, int size,
	     struct disassemble_info *info)
{
  int status;

  /* Get first byte.  Only one at a time because we don't know the
     size of the insn.  */
  status = (*info->read_memory_func) (memaddr, buffer, size, info);
  if (status != 0)
    {
      (*info->memory_error_func) (status, memaddr, info);
      return -1;
    }
  return 0;
}

static int
sex5 (unsigned v)
{
  return (int) (v & 0x0f) - (int) (v & 0x10);
}

static int
sex8 (unsigned v)
{
  return (int) (v & 0x7f) - (int) (v & 0x80);
}

static int
sex16 (unsigned v)
{
  return (int) (v & 0x7fff) - (int) (v & 0x8000);
}

static void
print_stack (struct disassemble_info *info, char *altstack, unsigned arg)
{
  int b;
  arg &= 0xff;
  if (arg == 0)
    return;
  for (b = 0; b < 8; b++)
    {
      if (arg & (1 << b))
	{
	  const char *rstr = (b == 6) ? altstack : reg_stack[b];
	  (*info->fprintf_func) (info->stream, "%s", rstr);
	  arg &= ~(1 << b);
	  if (arg)
	    (*info->fprintf_func) (info->stream, ",");
	}
    }
}

/*
 * Disassemble one instruction at address 'memaddr'.  Returns the number of
 * bytes used by that instruction.
 */

static int
print_insn (bfd_vma memaddr, struct disassemble_info *info, int arch)
{
  int status;
  bfd_byte buffer[4];
  unsigned int code;
  long format, pos;
  const struct m6809_opcode *opcode;
  int i;

  /*
   * While not technically a legal form, a 6809 will ignore page switch
   * op-codes past the first one.  The pathological case is a memory map
   * full of 0x10 or 0x11, but set the limit at 4 here.
   */

  format = 0;
  pos = 0;
  do
    {
      if (pos > 4)
	{
	  (*info->fprintf_func) (info->stream, "fcb\t$%02x", buffer[0]);
	  return 1;
	}
      status = read_memory (memaddr, buffer, 1, info);
      if (status != 0)
	return status;
      if (format == 0)
	{
	  if (buffer[0] == 0x10)
	    format = FMT_PAGE2;
	  else if (buffer[0] == 0x11)
	    format = FMT_PAGE3;
	}
      memaddr++;
      pos++;
    }
  while (buffer[0] == 0x10 || buffer[0] == 0x11);

  code = buffer[0];

  /*
   * Scan the opcode table until we find the opcode with the
   * corresponding page.
   */

  opcode = m6809_opcodes;
  for (i = 0; i < m6809_num_opcodes; i++, opcode++)
    {

      if (opcode->opcode != code)
	continue;
      if ((opcode->format & M6809_PAGE_MASK) != format)
	continue;

      /* We have found the opcode.  Extract the operand and print it.  */
      (*info->fprintf_func) (info->stream, "%s", opcode->name);

      if ((opcode->format & M6809_ADDRMODE_MASK) != FMT_INHERENT)
	(*info->fprintf_func) (info->stream, "\t");

      switch (opcode->format & M6809_ADDRMODE_MASK)
	{

	case FMT_INHERENT:
	  return pos;

	case FMT_DIRECT:
	  if (read_memory (memaddr, buffer, 1, info) != 0)
	    return -1;
	  memaddr++;
	  pos++;
	  (*info->fprintf_func) (info->stream, "<$%02x", buffer[0]);
	  return pos;

	case FMT_IMM8:
	  if (read_memory (memaddr, buffer, 1, info) != 0)
	    return -1;
	  memaddr++;
	  pos++;
	  (*info->fprintf_func) (info->stream, "#$%02x", buffer[0]);
	  return pos;

	case FMT_IMM16:
	  if (read_memory (memaddr, buffer, 2, info) != 0)
	    return -1;
	  memaddr += 2;
	  pos += 2;
	  (*info->fprintf_func) (info->stream, "#$%02x%02x", buffer[0],
				 buffer[1]);
	  return pos;

	case FMT_EXT:
	  if (read_memory (memaddr, buffer, 2, info) != 0)
	    return -1;
	  memaddr += 2;
	  pos += 2;
	  (*info->fprintf_func) (info->stream, "$%02x%02x", buffer[0],
				 buffer[1]);
	  return pos;

	case FMT_REL8:
	  if (read_memory (memaddr, buffer, 1, info) != 0)
	    return -1;
	  memaddr++;
	  pos++;
	  (*info->fprintf_func) (info->stream, "$%04x",
				 (unsigned) (memaddr +
					     sex8 (buffer[0])) & 0xffff);
	  return pos;

	case FMT_REL16:
	  if (read_memory (memaddr, buffer, 2, info) != 0)
	    return -1;
	  memaddr += 2;
	  pos += 2;
	  (*info->fprintf_func) (info->stream, "$%04x",
				 (unsigned) (memaddr +
					     ((buffer[0] << 8) | buffer[1])) &
				 0xffff);
	  return pos;

	case FMT_REG_PAIR:
	  if (read_memory (memaddr, buffer, 1, info) != 0)
	    return -1;
	  memaddr++;
	  pos++;
	  {
	    int r0 = (buffer[0] >> 4) & 15;
	    int r1 = buffer[0] & 15;
	    if (arch == CPU6309)
	      {
		(*info->fprintf_func) (info->stream, "%s,%s",
				       reg_pair_6309[r0], reg_pair_6309[r1]);
	      }
	    else
	      {
		(*info->fprintf_func) (info->stream, "%s,%s",
				       reg_pair_6809[r0], reg_pair_6809[r1]);
	      }
	  }
	  return pos;

	case FMT_STACK_S:
	  if (read_memory (memaddr, buffer, 1, info) != 0)
	    return -1;
	  memaddr++;
	  pos++;
	  print_stack (info, "u", buffer[0]);
	  return pos;

	case FMT_STACK_U:
	  if (read_memory (memaddr, buffer, 1, info) != 0)
	    return -1;
	  memaddr++;
	  pos++;
	  print_stack (info, "s", buffer[0]);
	  return pos;

	  /* The complicated one - indexed addressing. */

	case FMT_IND:
	  {
	    unsigned int postbyte;
	    const char *ireg;
	    unsigned int value = 0;

	    if (read_memory (memaddr, buffer, 1, info) != 0)
	      return -1;
	    memaddr++;
	    pos++;
	    postbyte = buffer[0];
	    ireg = reg_indexed[(postbyte >> 5) & 3];

	    if (!(postbyte & 0x80))
	      {
		(*info->fprintf_func) (info->stream, "%d,%s", sex5 (postbyte),
				       ireg);
		return pos;
	      }

	    if ((postbyte & 15) == 8 || (postbyte & 15) == 12)
	      {
		if (read_memory (memaddr, buffer, 1, info) != 0)
		  return -1;
		memaddr++;
		pos++;
		value = buffer[0];
	      }
	    else if ((postbyte & 15) == 9 || (postbyte & 15) == 13
		     || (postbyte & 15) == 15)
	      {
		if (read_memory (memaddr, buffer, 2, info) != 0)
		  return -1;
		memaddr += 2;
		pos += 2;
		value = (buffer[0] << 8) | buffer[1];
	      }

	    /* TODO: the 6309 fleshes these out somewhat.  */

	    if (postbyte & 0x10)
	      (*info->fprintf_func) (info->stream, "[");

	    switch (postbyte & 0x0f)
	      {
	      case 0x00:
		(*info->fprintf_func) (info->stream, ",%s+", ireg);
		break;
	      case 0x01:
		(*info->fprintf_func) (info->stream, ",%s++", ireg);
		break;
	      case 0x02:
		(*info->fprintf_func) (info->stream, ",-%s", ireg);
		break;
	      case 0x03:
		(*info->fprintf_func) (info->stream, ",--%s", ireg);
		break;
	      case 0x04:
		(*info->fprintf_func) (info->stream, ",%s", ireg);
		break;
	      case 0x05:
		(*info->fprintf_func) (info->stream, "b,%s", ireg);
		break;
	      case 0x06:
		(*info->fprintf_func) (info->stream, "a,%s", ireg);
		break;
	      case 0x08:
		(*info->fprintf_func) (info->stream, "%d,%s", sex8 (value),
				       ireg);
		break;
	      case 0x09:
		(*info->fprintf_func) (info->stream, "%d,%s", sex16 (value),
				       ireg);
		break;
	      case 0x0b:
		(*info->fprintf_func) (info->stream, "d,%s", ireg);
		break;
	      case 0x0c:
		(*info->fprintf_func) (info->stream, "$%04x,PCR",
				       (unsigned) (memaddr +
						   sex8 (value)) & 0xffff);
		break;
	      case 0x0d:
		(*info->fprintf_func) (info->stream, "$%04x,PCR",
				       (unsigned) (memaddr + value) & 0xffff);
		break;
	      case 0x0f:
		(*info->fprintf_func) (info->stream, "$%04x", value);
		break;
	      default:
		break;
	      }

	    if (postbyte & 0x10)
	      (*info->fprintf_func) (info->stream, "]");

	  }
	  return pos;

	default:
	  return pos;

	}

    }

  /* Opcode not recognized.  */
  (*info->fprintf_func) (info->stream, "fcb\t$%02x", code);

  return pos;
}

/* Disassemble one instruction at address 'memaddr'.  Returns the number
   of bytes used by that instruction.  */
int
print_insn_m6809 (bfd_vma memaddr, struct disassemble_info *info)
{
  return print_insn (memaddr, info, CPU6809);
}
