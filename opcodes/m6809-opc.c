/* m6809-opc.c -- Motorola 6809 opcode list
   Copyright 2013
   Free Software Foundation, Inc.
   Written by Ciaran Anscomb <xroar@6809.org.uk>

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this file; see the file COPYING.  If not, write to the
   Free Software Foundation, 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* TODO: HD6309 extensions */

#include <stdio.h>

#include "ansidecl.h"
#include "opcode/m6809.h"

#define TABLE_SIZE(X)   (sizeof(X) / sizeof(X[0]))

#define FMT_PAGE2       M6809_FMT_PAGE2
#define FMT_PAGE3       M6809_FMT_PAGE3

#define FMT_INHERENT    M6809_FMT_INHERENT
#define FMT_DIRECT      M6809_FMT_DIRECT
#define FMT_IMM8        M6809_FMT_IMMEDIATE8
#define FMT_IMM16       M6809_FMT_IMMEDIATE16
#define FMT_IND         M6809_FMT_INDEXED
#define FMT_EXT         M6809_FMT_EXTENDED
#define FMT_REL8        M6809_FMT_RELATIVE8
#define FMT_REL16       M6809_FMT_RELATIVE16
#define FMT_REG_PAIR    M6809_FMT_REG_PAIR
#define FMT_STACK_S     M6809_FMT_STACK_S
#define FMT_STACK_U     M6809_FMT_STACK_U

const struct m6809_opcode m6809_opcodes[] = {

  {"neg", FMT_DIRECT, 0x00, 0},
  {"com", FMT_DIRECT, 0x03, 0},
  {"lsr", FMT_DIRECT, 0x04, 0},
  {"ror", FMT_DIRECT, 0x06, 0},
  {"asr", FMT_DIRECT, 0x07, 0},
  {"lsl", FMT_DIRECT, 0x08, 0},
  {"rol", FMT_DIRECT, 0x09, 0},
  {"dec", FMT_DIRECT, 0x0a, 0},
  {"inc", FMT_DIRECT, 0x0c, 0},
  {"tst", FMT_DIRECT, 0x0d, 0},
  {"jmp", FMT_DIRECT, 0x0e, 0},
  {"clr", FMT_DIRECT, 0x0f, 0},

  {"nop", FMT_INHERENT, 0x12, 0},
  {"sync", FMT_INHERENT, 0x13, 0},
  {"lbra", FMT_REL16, 0x16, 0},
  {"lbsr", FMT_REL16, 0x17, 0},
  {"daa", FMT_INHERENT, 0x19, 0},
  {"orcc", FMT_IMM8, 0x1a, 0},
  {"andcc", FMT_IMM8, 0x1c, 0},
  {"sex", FMT_INHERENT, 0x1d, 0},
  {"exg", FMT_REG_PAIR, 0x1e, 0},
  {"tfr", FMT_REG_PAIR, 0x1f, 0},

  {"bra", FMT_REL8, 0x20, 0},
  {"brn", FMT_REL8, 0x21, 0},
  {"bhi", FMT_REL8, 0x22, 0},
  {"bls", FMT_REL8, 0x23, 0},
  {"bcc", FMT_REL8, 0x24, 0},
  {"bcs", FMT_REL8, 0x25, 0},
  {"bne", FMT_REL8, 0x26, 0},
  {"beq", FMT_REL8, 0x27, 0},
  {"bvc", FMT_REL8, 0x28, 0},
  {"bvs", FMT_REL8, 0x29, 0},
  {"bpl", FMT_REL8, 0x2a, 0},
  {"bmi", FMT_REL8, 0x2b, 0},
  {"bge", FMT_REL8, 0x2c, 0},
  {"blt", FMT_REL8, 0x2d, 0},
  {"bgt", FMT_REL8, 0x2e, 0},
  {"ble", FMT_REL8, 0x2f, 0},

  {"leax", FMT_IND, 0x30, 0},
  {"leay", FMT_IND, 0x31, 0},
  {"leas", FMT_IND, 0x32, 0},
  {"leau", FMT_IND, 0x33, 0},
  {"pshs", FMT_STACK_S, 0x34, 0},
  {"puls", FMT_STACK_S, 0x35, 0},
  {"pshu", FMT_STACK_U, 0x36, 0},
  {"pulu", FMT_STACK_U, 0x37, 0},
  {"rts", FMT_INHERENT, 0x39, 0},
  {"abx", FMT_INHERENT, 0x3a, 0},
  {"rti", FMT_INHERENT, 0x3b, 0},
  {"cwai", FMT_IMM8, 0x3c, 0},
  {"mul", FMT_INHERENT, 0x3d, 0},
  {"swi", FMT_INHERENT, 0x3f, 0},

  {"nega", FMT_INHERENT, 0x40, 0},
  {"coma", FMT_INHERENT, 0x43, 0},
  {"lsra", FMT_INHERENT, 0x44, 0},
  {"rora", FMT_INHERENT, 0x46, 0},
  {"asra", FMT_INHERENT, 0x47, 0},
  {"lsla", FMT_INHERENT, 0x48, 0},
  {"rola", FMT_INHERENT, 0x49, 0},
  {"deca", FMT_INHERENT, 0x4a, 0},
  {"inca", FMT_INHERENT, 0x4c, 0},
  {"tsta", FMT_INHERENT, 0x4d, 0},
  {"clra", FMT_INHERENT, 0x4f, 0},

  {"negb", FMT_INHERENT, 0x50, 0},
  {"comb", FMT_INHERENT, 0x53, 0},
  {"lsrb", FMT_INHERENT, 0x54, 0},
  {"rorb", FMT_INHERENT, 0x56, 0},
  {"asrb", FMT_INHERENT, 0x57, 0},
  {"lslb", FMT_INHERENT, 0x58, 0},
  {"rolb", FMT_INHERENT, 0x59, 0},
  {"decb", FMT_INHERENT, 0x5a, 0},
  {"incb", FMT_INHERENT, 0x5c, 0},
  {"tstb", FMT_INHERENT, 0x5d, 0},
  {"clrb", FMT_INHERENT, 0x5f, 0},

  {"neg", FMT_IND, 0x60, 0},
  {"com", FMT_IND, 0x63, 0},
  {"lsr", FMT_IND, 0x64, 0},
  {"ror", FMT_IND, 0x66, 0},
  {"asr", FMT_IND, 0x67, 0},
  {"lsl", FMT_IND, 0x68, 0},
  {"rol", FMT_IND, 0x69, 0},
  {"dec", FMT_IND, 0x6a, 0},
  {"inc", FMT_IND, 0x6c, 0},
  {"tst", FMT_IND, 0x6d, 0},
  {"jmp", FMT_IND, 0x6e, 0},
  {"clr", FMT_IND, 0x6f, 0},

  {"neg", FMT_EXT, 0x70, 0},
  {"com", FMT_EXT, 0x73, 0},
  {"lsr", FMT_EXT, 0x74, 0},
  {"ror", FMT_EXT, 0x76, 0},
  {"asr", FMT_EXT, 0x77, 0},
  {"lsl", FMT_EXT, 0x78, 0},
  {"rol", FMT_EXT, 0x79, 0},
  {"dec", FMT_EXT, 0x7a, 0},
  {"inc", FMT_EXT, 0x7c, 0},
  {"tst", FMT_EXT, 0x7d, 0},
  {"jmp", FMT_EXT, 0x7e, 0},
  {"clr", FMT_EXT, 0x7f, 0},

  {"suba", FMT_IMM8, 0x80, 0},
  {"cmpa", FMT_IMM8, 0x81, 0},
  {"sbca", FMT_IMM8, 0x82, 0},
  {"subd", FMT_IMM16, 0x83, 0},
  {"anda", FMT_IMM8, 0x84, 0},
  {"bita", FMT_IMM8, 0x85, 0},
  {"lda", FMT_IMM8, 0x86, 0},
  {"eora", FMT_IMM8, 0x88, 0},
  {"adca", FMT_IMM8, 0x89, 0},
  {"ora", FMT_IMM8, 0x8a, 0},
  {"adda", FMT_IMM8, 0x8b, 0},
  {"cmpx", FMT_IMM16, 0x8c, 0},
  {"bsr", FMT_REL8, 0x8d, 0},
  {"ldx", FMT_IMM16, 0x8e, 0},

  {"suba", FMT_DIRECT, 0x90, 0},
  {"cmpa", FMT_DIRECT, 0x91, 0},
  {"sbca", FMT_DIRECT, 0x92, 0},
  {"subd", FMT_DIRECT, 0x93, 0},
  {"anda", FMT_DIRECT, 0x94, 0},
  {"bita", FMT_DIRECT, 0x95, 0},
  {"lda", FMT_DIRECT, 0x96, 0},
  {"sta", FMT_DIRECT, 0x97, 0},
  {"eora", FMT_DIRECT, 0x98, 0},
  {"adca", FMT_DIRECT, 0x99, 0},
  {"ora", FMT_DIRECT, 0x9a, 0},
  {"adda", FMT_DIRECT, 0x9b, 0},
  {"cmpx", FMT_DIRECT, 0x9c, 0},
  {"jsr", FMT_DIRECT, 0x9d, 0},
  {"ldx", FMT_DIRECT, 0x9e, 0},
  {"stx", FMT_DIRECT, 0x9f, 0},

  {"suba", FMT_IND, 0xa0, 0},
  {"cmpa", FMT_IND, 0xa1, 0},
  {"sbca", FMT_IND, 0xa2, 0},
  {"subd", FMT_IND, 0xa3, 0},
  {"anda", FMT_IND, 0xa4, 0},
  {"bita", FMT_IND, 0xa5, 0},
  {"lda", FMT_IND, 0xa6, 0},
  {"sta", FMT_IND, 0xa7, 0},
  {"eora", FMT_IND, 0xa8, 0},
  {"adca", FMT_IND, 0xa9, 0},
  {"ora", FMT_IND, 0xaa, 0},
  {"adda", FMT_IND, 0xab, 0},
  {"cmpx", FMT_IND, 0xac, 0},
  {"jsr", FMT_IND, 0xad, 0},
  {"ldx", FMT_IND, 0xae, 0},
  {"stx", FMT_IND, 0xaf, 0},

  {"suba", FMT_EXT, 0xb0, 0},
  {"cmpa", FMT_EXT, 0xb1, 0},
  {"sbca", FMT_EXT, 0xb2, 0},
  {"subd", FMT_EXT, 0xb3, 0},
  {"anda", FMT_EXT, 0xb4, 0},
  {"bita", FMT_EXT, 0xb5, 0},
  {"lda", FMT_EXT, 0xb6, 0},
  {"sta", FMT_EXT, 0xb7, 0},
  {"eora", FMT_EXT, 0xb8, 0},
  {"adca", FMT_EXT, 0xb9, 0},
  {"ora", FMT_EXT, 0xba, 0},
  {"adda", FMT_EXT, 0xbb, 0},
  {"cmpx", FMT_EXT, 0xbc, 0},
  {"jsr", FMT_EXT, 0xbd, 0},
  {"ldx", FMT_EXT, 0xbe, 0},
  {"stx", FMT_EXT, 0xbf, 0},

  {"subb", FMT_IMM8, 0xc0, 0},
  {"cmpb", FMT_IMM8, 0xc1, 0},
  {"sbcb", FMT_IMM8, 0xc2, 0},
  {"addd", FMT_IMM16, 0xc3, 0},
  {"andb", FMT_IMM8, 0xc4, 0},
  {"bitb", FMT_IMM8, 0xc5, 0},
  {"ldb", FMT_IMM8, 0xc6, 0},
  {"eorb", FMT_IMM8, 0xc8, 0},
  {"adcb", FMT_IMM8, 0xc9, 0},
  {"orb", FMT_IMM8, 0xca, 0},
  {"addb", FMT_IMM8, 0xcb, 0},
  {"ldd", FMT_IMM16, 0xcc, 0},
  {"ldu", FMT_IMM16, 0xce, 0},

  {"subb", FMT_DIRECT, 0xd0, 0},
  {"cmpb", FMT_DIRECT, 0xd1, 0},
  {"sbcb", FMT_DIRECT, 0xd2, 0},
  {"addd", FMT_DIRECT, 0xd3, 0},
  {"andb", FMT_DIRECT, 0xd4, 0},
  {"bitb", FMT_DIRECT, 0xd5, 0},
  {"ldb", FMT_DIRECT, 0xd6, 0},
  {"stb", FMT_DIRECT, 0xd7, 0},
  {"eorb", FMT_DIRECT, 0xd8, 0},
  {"adcb", FMT_DIRECT, 0xd9, 0},
  {"orb", FMT_DIRECT, 0xda, 0},
  {"addb", FMT_DIRECT, 0xdb, 0},
  {"ldd", FMT_DIRECT, 0xdc, 0},
  {"std", FMT_DIRECT, 0xdd, 0},
  {"ldu", FMT_DIRECT, 0xde, 0},
  {"stu", FMT_DIRECT, 0xdf, 0},

  {"subb", FMT_IND, 0xe0, 0},
  {"cmpb", FMT_IND, 0xe1, 0},
  {"sbcb", FMT_IND, 0xe2, 0},
  {"addd", FMT_IND, 0xe3, 0},
  {"andb", FMT_IND, 0xe4, 0},
  {"bitb", FMT_IND, 0xe5, 0},
  {"ldb", FMT_IND, 0xe6, 0},
  {"stb", FMT_IND, 0xe7, 0},
  {"eorb", FMT_IND, 0xe8, 0},
  {"adcb", FMT_IND, 0xe9, 0},
  {"orb", FMT_IND, 0xea, 0},
  {"addb", FMT_IND, 0xeb, 0},
  {"ldd", FMT_IND, 0xec, 0},
  {"std", FMT_IND, 0xed, 0},
  {"ldu", FMT_IND, 0xee, 0},
  {"stu", FMT_IND, 0xef, 0},

  {"subb", FMT_EXT, 0xf0, 0},
  {"cmpb", FMT_EXT, 0xf1, 0},
  {"sbcb", FMT_EXT, 0xf2, 0},
  {"addd", FMT_EXT, 0xf3, 0},
  {"andb", FMT_EXT, 0xf4, 0},
  {"bitb", FMT_EXT, 0xf5, 0},
  {"ldb", FMT_EXT, 0xf6, 0},
  {"stb", FMT_EXT, 0xf7, 0},
  {"eorb", FMT_EXT, 0xf8, 0},
  {"adcb", FMT_EXT, 0xf9, 0},
  {"orb", FMT_EXT, 0xfa, 0},
  {"addb", FMT_EXT, 0xfb, 0},
  {"ldd", FMT_EXT, 0xfc, 0},
  {"std", FMT_EXT, 0xfd, 0},
  {"ldu", FMT_EXT, 0xfe, 0},
  {"stu", FMT_EXT, 0xff, 0},

  {"lbra", FMT_PAGE2 | FMT_REL16, 0x20, 0},
  {"lbrn", FMT_PAGE2 | FMT_REL16, 0x21, 0},
  {"lbhi", FMT_PAGE2 | FMT_REL16, 0x22, 0},
  {"lbls", FMT_PAGE2 | FMT_REL16, 0x23, 0},
  {"lbcc", FMT_PAGE2 | FMT_REL16, 0x24, 0},
  {"lbcs", FMT_PAGE2 | FMT_REL16, 0x25, 0},
  {"lbne", FMT_PAGE2 | FMT_REL16, 0x26, 0},
  {"lbeq", FMT_PAGE2 | FMT_REL16, 0x27, 0},
  {"lbvc", FMT_PAGE2 | FMT_REL16, 0x28, 0},
  {"lbvs", FMT_PAGE2 | FMT_REL16, 0x29, 0},
  {"lbpl", FMT_PAGE2 | FMT_REL16, 0x2a, 0},
  {"lbmi", FMT_PAGE2 | FMT_REL16, 0x2b, 0},
  {"lbge", FMT_PAGE2 | FMT_REL16, 0x2c, 0},
  {"lblt", FMT_PAGE2 | FMT_REL16, 0x2d, 0},
  {"lbgt", FMT_PAGE2 | FMT_REL16, 0x2e, 0},
  {"lble", FMT_PAGE2 | FMT_REL16, 0x2f, 0},

  {"swi2", FMT_PAGE2 | FMT_INHERENT, 0x3f, 0},

  {"cmpd", FMT_PAGE2 | FMT_IMM16, 0x83, 0},
  {"cmpy", FMT_PAGE2 | FMT_IMM16, 0x8c, 0},
  {"ldy", FMT_PAGE2 | FMT_IMM16, 0x8e, 0},

  {"cmpd", FMT_PAGE2 | FMT_DIRECT, 0x93, 0},
  {"cmpy", FMT_PAGE2 | FMT_DIRECT, 0x9c, 0},
  {"ldy", FMT_PAGE2 | FMT_DIRECT, 0x9e, 0},
  {"sty", FMT_PAGE2 | FMT_DIRECT, 0x9f, 0},

  {"cmpd", FMT_PAGE2 | FMT_IND, 0xa3, 0},
  {"cmpy", FMT_PAGE2 | FMT_IND, 0xac, 0},
  {"ldy", FMT_PAGE2 | FMT_IND, 0xae, 0},
  {"sty", FMT_PAGE2 | FMT_IND, 0xaf, 0},

  {"cmpd", FMT_PAGE2 | FMT_EXT, 0xb3, 0},
  {"cmpy", FMT_PAGE2 | FMT_EXT, 0xbc, 0},
  {"ldy", FMT_PAGE2 | FMT_EXT, 0xbe, 0},
  {"sty", FMT_PAGE2 | FMT_EXT, 0xbf, 0},

  {"lds", FMT_PAGE2 | FMT_IMM16, 0xce, 0},
  {"lds", FMT_PAGE2 | FMT_DIRECT, 0xde, 0},
  {"sts", FMT_PAGE2 | FMT_DIRECT, 0xdf, 0},
  {"lds", FMT_PAGE2 | FMT_IND, 0xee, 0},
  {"sts", FMT_PAGE2 | FMT_IND, 0xef, 0},
  {"lds", FMT_PAGE2 | FMT_EXT, 0xfe, 0},
  {"sts", FMT_PAGE2 | FMT_EXT, 0xff, 0},

  {"swi3", FMT_PAGE3 | FMT_INHERENT, 0x3f, 0},

  {"cmpu", FMT_PAGE3 | FMT_IMM16, 0x83, 0},
  {"cmps", FMT_PAGE3 | FMT_IMM16, 0x8c, 0},

  {"cmpu", FMT_PAGE3 | FMT_DIRECT, 0x93, 0},
  {"cmps", FMT_PAGE3 | FMT_DIRECT, 0x9c, 0},

  {"cmpu", FMT_PAGE3 | FMT_IND, 0xa3, 0},
  {"cmps", FMT_PAGE3 | FMT_IND, 0xac, 0},

  {"cmpu", FMT_PAGE3 | FMT_EXT, 0xb3, 0},
  {"cmps", FMT_PAGE3 | FMT_EXT, 0xbc, 0},

};

const int m6809_num_opcodes = TABLE_SIZE (m6809_opcodes);
